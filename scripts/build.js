#!/usr/bin/env node

/* global npm_package_name, npm_package_version */

const fs = require('fs'),
      concat = require("concat-files"),
      uglify = require("uglify-js");

const rootDir = __dirname.replace("/scripts","/"),
      distDir = `${rootDir}dist/`,
      outFile = `${distDir}/${process.env.npm_package_name}-${process.env.npm_package_version}.js`;
      
//create dist folder if not exists      
if(!fs.existsSync(distDir)) {
    fs.mkdirSync(distDir);
}
      
concat(
    [
        `${rootDir}components/folder.js`,
        `${rootDir}components/file.js`
    ],
    outFile, 
    (err) => {
        if(err) throw err;
    
    });