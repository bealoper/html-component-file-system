/* global HTMLElement  */

(function(w,d){

    class FileElement extends HTMLElement {
         constructor() {
            super();
            
        }
        
        connectedCallback() {
            const shadowRoot = this.attachShadow({mode: 'open'}),
                  href = this.getAttribute("href") || "#",
                  target = this.getAttribute("target") || "_self";
            shadowRoot.innerHTML = this._render(this.title, href, target);
           
            this.addEventListener("click", (e) => {
                
            }, true);
        }
        
        get href() {
            return this.getAttribute("href");
        }
        
        get target() {
            return this.getAttribute("target");
        }
        
        set href(value) {
            this.setAttribute("href", value);
        } 
        
        set target(value) {
            this.setAttribute("target", value);
        }
        
        _render(label, href, target) {
            return (`
                <style type="text/css">
                    :host {
                        display:block;
                    }
                    
                    :host .label {
                        position:relative;
                        padding-left:1.75em;
                    }
                    
                    :host a {
                        color:inherit;
                        text-decoration:none;
                    }
                    
                    :host .icon {
                        line-height:1em;
                        font-size:1.2em;
                        position:absolute;
                        left:.2em;
                    }
                </style>
                
                <div class="label">
                    <a href="${href}" target="${target}">
                        <span class="icon">&#128459;</span>
                        ${label}
                    </a>
                </div>
            `);
        }
    }
    
    w.customElements.define('csit-file', FileElement, {extends: "a"});

})(window, document);