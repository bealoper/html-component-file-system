/* global HTMLElement */

(function(w,d){
    
    class FolderElement extends HTMLElement {
         constructor() {
            super();
            
            this.open = false;
            this.icon = null;
            this.contents = null;
            
        }
        
        connectedCallback() {
            const shadowRoot = this.attachShadow({mode: 'open'});
            shadowRoot.innerHTML = this._render(this.title);

            this.transitionTime = {open:250,close:150};
            this.open = this.hasAttribute("open");
            this.icon = shadowRoot.querySelector(".icon");
            this.contents = shadowRoot.querySelector(".contents");
            this._toggleContents(this.open);
            this._toggleIcon(this.open);
            
            this.addEventListener("click", (e) => {
                if(e.target === this){
                    e.stopPropagation();
                    this._toggle();
                }
            }, true);
        }
        
        _toggle() {
            this.open = !this.open;
            this._toggleContents(this.open);
            this._toggleIcon(this.open);
            (this.open) ? this.setAttribute("open","") : this.removeAttribute("open");
        }
        
        _toggleContents(open) {
            this.contents.className = `contents ${(open) ? "open" : ""}`;
        }
        
        _toggleIcon(open) {
            if(open) {
                setTimeout((icon) => {
                    icon.innerHTML = "&#128449;";
                }, 0, this.icon);
            } else {
                setTimeout((icon) => {
                    icon.innerHTML = "&#128448;";
                }, this.transitionTime.close, this.icon);
            }
        }
        
        _render(label) {
            return (`
                <style type="text/css">
                    :host {
                        display:block;
                    }
                    
                    :host .label {
                        cursor:pointer;
                        position:relative;
                        padding-left:1.75em;
                    }
                    
                    :host .icon {
                        line-height:1em;
                        font-size:1.2em;
                        position:absolute;
                        left:0;
                    }
                    
                    :host .contents{
                        margin-left:1.75em;
                        max-height: 0;
                        transition: max-height 150ms ease-out;
                        overflow: hidden;
                    }
                    :host .contents.open {
                        max-height: 500px;
                        transition: max-height 250ms ease-in;
                    }
                </style>
    
                <div class="label">
                    <span class="icon">&#128448;</span>
                    ${label}
                </div>
                <div class="contents">
                    <slot></slot>
                </div>
                
            `);
        }
    }
    
    w.customElements.define('csit-folder', FolderElement);

})(window, document);