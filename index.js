import { Folder } from "components/folder";
import { File } from "components/file";

module.exports.Folder = Folder;
module.exports.File = File;